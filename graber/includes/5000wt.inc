<?php

function graber_baseurl_5000wt(){
    return "http://5000wt.ru";
}
function graber_pager_5000wt(){
    return "?PAGEN_1=";

}

function graber_products_5000wt(){
    $base_url=graber_baseurl_5000wt();
    $products=pq(".catalog_table")->find(".tovar_block");
    $links=array();
    foreach($products as $prod){
        $url=pq($prod)->find(".title a");
        $price=pq($prod)->find('.td_price')->text();
        $price=preg_replace("/[^0-9]/","",$price);
        $links[]=array(
            'href'=>$base_url.$url->attr('href'),
            'name'=>$url->text(),
            'cost'=>$price,
        );
    }

    return $links;

}

function graber_product_info_5000wt($document){
    $base_url=graber_baseurl_5000wt();
    $photo=pq("div.big_foto img")->attr('src');
    $info=array(
        'photo'=>$base_url.$photo,
        'teh'=>pq("div.table_key")->html(),
        'body'=>pq('div.overview .text-content')->html(),
    );
    return $info;
}

function graber_teh_5000wt($src){
   $doc=phpQuery::newDocument($src);
   $params=array();
   foreach($doc->find('tr') as $tr){
       $param=pq($tr)->find('td')->eq(0)->text();
       $value=pq($tr)->find('td')->eq(1)->text();
    $params[]=array('param'=>$param,'value'=>$value);

   }
    return $params;
}